import Vue from 'vue';
import Router from 'vue-router';
import TextEditor from '@/components/TextEditor';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'TextEditor',
      component: TextEditor
    }
  ]
});
