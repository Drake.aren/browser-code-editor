const express = require('express');
const app = express();
const port = 3000;

const fs = require('fs');
app.get('/', (req, res) => {
  res.send('/');
});

app.get('/files', (req, res) => {
  let path = req.query.path;
  let items = fs.readdirSync(path, {
    withFileTypes: true
  });
  res.json(items);
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
